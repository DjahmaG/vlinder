package be.viermusketiers.vlinder.entities.relation;

import be.viermusketiers.vlinder.entities.user.TestingUserRepository;
import be.viermusketiers.vlinder.entities.user.User;
import be.viermusketiers.vlinder.entities.user.service.UserMatchMaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DummyRelationBuilder {
    @Autowired
    TestingUserRepository userRepository;

    private User aLikesBAndC;
    private UserMatchMaker aMatchMaker;
    private User bLikesA;
    private UserMatchMaker bMatchMaker;
    private User cLikesB;
    private UserMatchMaker cMatchMaker;


    public void makeUsersAndMatchmakersABC() {
        aLikesBAndC = userRepository.makeAndSaveDummyUser("A");
        aMatchMaker = new UserMatchMaker(aLikesBAndC);
        bLikesA = userRepository.makeAndSaveDummyUser("B");
        bMatchMaker = new UserMatchMaker(bLikesA);
        cLikesB = userRepository.makeAndSaveDummyUser("C");
        cMatchMaker = new UserMatchMaker(cLikesB);
    }

    public void likeAllLikedABCUsers() {
        buildLikeRelationsForA();
        buildLikeRelationForB();
        buildLikeRelationForC();
    }

    public void buildLikeRelationsForA() {
        aMatchMaker.judgeUserAndSave(bLikesA, RelationType.LIKE);
        aMatchMaker.judgeUserAndSave(cLikesB, RelationType.LIKE);
    }

    public void buildLikeRelationForB() {
        bMatchMaker.judgeUserAndSave(aLikesBAndC, RelationType.LIKE);
    }

    public void buildLikeRelationForC() {
        cMatchMaker.judgeUserAndSave(bLikesA, RelationType.LIKE);
    }

    public void dislikeNonLikedABCUsers() {
        bMatchMaker.judgeUserAndSave(cLikesB, RelationType.DISLIKE);
        cMatchMaker.judgeUserAndSave(aLikesBAndC, RelationType.DISLIKE);
    }

    public User getaLikesBAndC() {
        return aLikesBAndC;
    }

    public User getbLikesA() {
        return bLikesA;
    }

    public User getcLikesB() {
        return cLikesB;
    }
}
