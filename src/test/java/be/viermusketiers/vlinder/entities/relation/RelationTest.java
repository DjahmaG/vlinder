package be.viermusketiers.vlinder.entities.relation;

import be.viermusketiers.vlinder.VlinderApplication;
import be.viermusketiers.vlinder.entities.user.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest(classes = {VlinderApplication.class, RelationRepository.class})
public class RelationTest {
    @Autowired
    private RelationRepository relationRepository;
    @Autowired
    private DummyRelationBuilder relationBuilder;

    private User a, b, c;

    @BeforeEach
    private void resetUsers() {
        a = null;
        b = null;
        c = null;
    }

    @Test
    public void testConnectionByGettingAllRelations() {
        relationRepository.getAllRelations();
    }

    @Test
    @Transactional
    public void testIfRelationsAreSaved() {
        buildRelationsAndGetUsers();

        Assertions.assertTrue(
                relationRepository.userLikesUser(a, b) &&
                        relationRepository.userLikesUser(a, c) &&
                        relationRepository.userLikesUser(c, b) &&
                        relationRepository.userLikesUser(b, a)
        );
    }

    @Test
    @Transactional
    public void testGetLikedUsers() {
        buildRelationsAndGetUsers();

        Assertions.assertTrue(
                relationRepository.getLikedUsers(relationBuilder.getaLikesBAndC())
                        .contains(relationBuilder.getbLikesA())
        );
    }

    @Test
    @Transactional
    public void testGetMatches() {
        buildRelationsAndGetUsers();

        List<User> matchesOfA = a.getNewUserMatchMaker().getMatchedUsers(),
                matchesOfB = b.getNewUserMatchMaker().getMatchedUsers(),
                matchesOfC = c.getNewUserMatchMaker().getMatchedUsers();

        Assertions.assertTrue(
                matchesOfA.contains(b) &&
                        !matchesOfA.contains(c) &&
                        matchesOfB.contains(a) &&
                        !matchesOfB.contains(c) &&
                        matchesOfC.isEmpty()
        );
    }

    @Test
    @Transactional
    public void testGetUnratedUsers() {
        buildAndGetUsers();
        relationBuilder.likeAllLikedABCUsers();

        List<User> unratedUsersOfA = a.getNewUserMatchMaker().getUnratedUsers(),
                unratedUsersOfB = b.getNewUserMatchMaker().getUnratedUsers(),
                unratedUsersOfC = c.getNewUserMatchMaker().getUnratedUsers();

        Assertions.assertFalse(unratedUsersOfA.contains(a) ||
                        unratedUsersOfB.contains(b) ||
                        unratedUsersOfC.contains(c),
                "You shouldn't be able to rate yourself");
        Assertions.assertTrue(!unratedUsersOfA.contains(b) &&
                        !unratedUsersOfA.contains(c),
                "A should have already rated B and C");
        Assertions.assertTrue(!unratedUsersOfB.contains(a) &&
                        unratedUsersOfB.contains(c),
                "B should have already liked A and be able to rate C");
        Assertions.assertTrue(unratedUsersOfC.contains(a) &&
                        !unratedUsersOfC.contains(b),
                "C should have already liked B and be able to rate A"
        );
    }

    private void buildRelationsAndGetUsers() {
        buildAndGetUsers();

        relationBuilder.likeAllLikedABCUsers();
        relationBuilder.dislikeNonLikedABCUsers();
    }

    private void buildAndGetUsers() {
        relationBuilder.makeUsersAndMatchmakersABC();

        a = relationBuilder.getaLikesBAndC();
        b = relationBuilder.getbLikesA();
        c = relationBuilder.getcLikesB();
    }

}
