package be.viermusketiers.vlinder.entities.message;

import be.viermusketiers.vlinder.entities.chat.Message;
import be.viermusketiers.vlinder.entities.user.TestingUserRepository;
import be.viermusketiers.vlinder.entities.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class DummyMessageBuilder {
    @Autowired
    TestingUserRepository userRepository;

    public Message makeDummyMessage() {
        User sender = userRepository.makeAndSaveDummyUser("Sender");
        User receiver = userRepository.makeAndSaveDummyUser("Reciever");

        Message message = new Message();
        message.setText("This is a test message");
        message.setSender(sender);
        message.setRetriever(receiver);
        message.setSendTime(LocalDateTime.now());
        message.setStatus(null);

        return message;
    }
}
