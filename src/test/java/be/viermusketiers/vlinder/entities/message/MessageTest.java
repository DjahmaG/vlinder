package be.viermusketiers.vlinder.entities.message;

import be.viermusketiers.vlinder.VlinderApplication;
import be.viermusketiers.vlinder.entities.chat.Message;
import be.viermusketiers.vlinder.entities.chat.MessageRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {VlinderApplication.class, MessageRepository.class})
class MessageTest {
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    DummyMessageBuilder dummyMessageBuilder;

    @Test
    public void testConnectionByGettingAllMessages() {
        messageRepository.getAllMessages();
    }

    @Test
    @Transactional
    public void testAddingOfNewMessageAndUser() {
        Message message = dummyMessageBuilder.makeDummyMessage();

        messageRepository.saveMessage(message);

        assertTrue(
            messageRepository.messageExists(message)
        );
    }



}