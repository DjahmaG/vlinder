package be.viermusketiers.vlinder.entities.user;

import be.viermusketiers.vlinder.entities.user.repository.UserRepository;

import java.time.LocalDate;

public interface TestingUserRepository extends UserRepository {

    default User makeAndSaveDummyUser(String name) {
        User user = makeDummyUser(name);
        saveUser(user);
        return user;
    }

    default User makeAndSaveDummyUser() {
        return makeAndSaveDummyUser("Bettie");
    }

    default User makeDummyUser(String name) {
        User user = new User();
        user.setName(name);
        user.setGender(Gender.FEMALE);
        user.setDateOfBirth(LocalDate.of(2018, 10, 7));
        user.setSpecies("Curly dog");
        user.setLocation("De Pinte");
        user.setProfilePicture(null);   // Wordt niet getest
        user.setDescription("Curly dog");
        user.setEmail(name + "@cooldog.com");
        user.setPassword("hondje123");

        return user;
    }

    default User makeDummyUser() {
        return makeDummyUser("Bettie");
    }
}
