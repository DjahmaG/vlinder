package be.viermusketiers.vlinder.entities.user;

import be.viermusketiers.vlinder.VlinderApplication;
import be.viermusketiers.vlinder.entities.user.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {VlinderApplication.class, UserRepository.class})
class UserTest {
    @Autowired
    private TestingUserRepository userRepository;

    @Test
    public void testConnectionByGettingAllUsers() {
        userRepository.getAllUsers();
    }

    @Test
    @Transactional
    public void testAddingOfNewUser() {
        User user = userRepository.makeAndSaveDummyUser();

        assertTrue(
                userRepository.userExists(user)
        );
    }

}