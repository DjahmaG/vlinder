open module Vlinder {
    requires java.persistence;              // JPA Entity

    requires spring.boot;                   // Needed to run SpringApplication
    requires spring.boot.autoconfigure;     //

    requires org.hibernate.orm.core;        // Verbinding met databank

    requires spring.data.jpa;               // Used for repositories

    requires spring.beans;                  // Usage of @Autowired
    requires spring.core;
    requires spring.context;

    requires spring.web;                    // Used for thymeleaf
    requires java.annotation;
    requires spring.boot.starter.web;
    requires spring.webmvc;
    requires org.apache.tomcat.embed.core;

    requires java.validation;               // Used for mapping html-fields to object attributes

    requires spring.security.core;          // Security
    requires spring.security.config;        //

    requires spring.security.crypto;        // PasswordEncoder
}