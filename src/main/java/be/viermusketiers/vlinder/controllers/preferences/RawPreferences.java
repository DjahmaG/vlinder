package be.viermusketiers.vlinder.controllers.preferences;

public class RawPreferences {
    public int distance;

    public boolean isMale;
    public boolean isFemale;
    public boolean isHermaphrodite;
    public boolean isOther;

    public boolean isMammal;
    public boolean isBird;
    public boolean isReptile;
    public boolean isAmphibian;
    public boolean isFish;
    public boolean isInvertebrate;
}
