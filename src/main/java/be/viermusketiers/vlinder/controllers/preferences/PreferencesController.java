package be.viermusketiers.vlinder.controllers.preferences;

import be.viermusketiers.vlinder.entities.preferences.repository.PreferencesUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping({"preferences", "Preferences"})
public class PreferencesController {
    @Autowired
    private PreferencesUserRepository preferencesUserRepository;

    @GetMapping
    public String getUserPreferences(HttpSession httpSession, Model model) {
        model.addAttribute("preferences", new RawPreferences());
        return "html/UserPreferences";
    }

    /*@PostMapping
    public String updatePreferences(HttpSession httpSession, )*/
}
