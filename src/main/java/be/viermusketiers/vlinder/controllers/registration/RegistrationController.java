package be.viermusketiers.vlinder.controllers.registration;

import be.viermusketiers.vlinder.entities.user.User;
import be.viermusketiers.vlinder.entities.user.service.UserRegistrator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping({"register", "Register"})
public class RegistrationController {
    @Autowired
    private UserRegistrator userRegistrator;
    public final static String REGISTRATION_PAGE = "registration/registerForm";
    public final static String TO_PROFILE = "redirect:/profile";

    @GetMapping
    public String showRegisterForm(Model model){
        User user = new User();
        model.addAttribute("user", user);
        return REGISTRATION_PAGE;
    }

    @PostMapping
    public String postRegistration(HttpSession session, User user, @RequestParam String unencodedPassword) {
        try {
            registerUserAndAddToSession(session, user, unencodedPassword);
            return TO_PROFILE;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return REGISTRATION_PAGE;
    }

    private void registerUserAndAddToSession(HttpSession session, User user, String unencodedPassword) {
        userRegistrator.registerUser(user, unencodedPassword);
        session.setAttribute("user", user);
    }
}
