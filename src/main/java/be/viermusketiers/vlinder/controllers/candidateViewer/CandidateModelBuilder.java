package be.viermusketiers.vlinder.controllers.candidateViewer;

import be.viermusketiers.vlinder.controllers.modelbuilder.ModelAndSessionBuilderWithUser;
import be.viermusketiers.vlinder.entities.relation.RelationType;
import be.viermusketiers.vlinder.entities.user.User;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.Period;

public class CandidateModelBuilder extends ModelAndSessionBuilderWithUser {
    private final User candidate;

    public CandidateModelBuilder(Model model, HttpSession session) {
        super(model, session);

        candidate = user.getNewUserMatchMaker().getPotentialCandidate();
    }

    @Override
    public void build() {
        addCandidateAgeAndRelationTypeToModelAndSession();
    }

    public final void addCandidateAgeAndRelationTypeToModelAndSession() {
        addCandidateToModelAndSession();
        addRelationTypeToModel();
    }

    private void addCandidateToModelAndSession() {
        int candidateAge = Period.between(candidate.getDateOfBirth(), LocalDate.now()).getYears();
        model.addAttribute("age", candidateAge);
        session.setAttribute("candidate",candidate);
        model.addAttribute("candidate", candidate);
    }

    private void addRelationTypeToModel() {
        RelationType judgement = RelationType.IGNORE;
        model.addAttribute("judgement", judgement);
    }
}
