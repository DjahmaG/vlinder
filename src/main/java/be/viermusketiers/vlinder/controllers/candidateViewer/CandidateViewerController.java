package be.viermusketiers.vlinder.controllers.candidateViewer;

import be.viermusketiers.vlinder.controllers.login.LoginController;
import be.viermusketiers.vlinder.entities.relation.RelationType;
import be.viermusketiers.vlinder.entities.user.User;
import be.viermusketiers.vlinder.util.exceptions.NoMoreUnratedUserException;
import be.viermusketiers.vlinder.util.exceptions.NoUserLoggedInException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("candidate")
public class CandidateViewerController {
    private static final String CANDIDATE_VIEWER = "html/candidateViewer";
    private static final String REDIRECT_CANDIDATE_VIEWER = "redirect:/candidate";
    private static final String REDIRECT_FAVORITES = "redirect:/favorites";

    @GetMapping
    public String nextCandidate(Model model, HttpSession session) {
        try {
            CandidateModelBuilder cg = new CandidateModelBuilder(model, session);
            cg.addCandidateAgeAndRelationTypeToModelAndSession();
        } catch (NoMoreUnratedUserException ignored) {
            return REDIRECT_FAVORITES;
        } catch (NoUserLoggedInException ignored) {
            return LoginController.REDIRECT_LOGIN;
        }

        return CANDIDATE_VIEWER;
    }

    @PostMapping
    public String judgeCandidateAndShowNext(HttpSession session, @RequestParam RelationType judgement){
        User user = (User) session.getAttribute("user");
        User candidate = (User) session.getAttribute("candidate");
        user.getNewUserMatchMaker().judgeUserAndSave(candidate, judgement);

        return REDIRECT_CANDIDATE_VIEWER;
    }
}
