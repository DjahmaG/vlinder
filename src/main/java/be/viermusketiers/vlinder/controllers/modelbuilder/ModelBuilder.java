package be.viermusketiers.vlinder.controllers.modelbuilder;

import org.springframework.ui.Model;

public abstract class ModelBuilder {
    protected final Model model;

    public ModelBuilder(Model model) {
        this.model = model;
    }

    public abstract void build();
}
