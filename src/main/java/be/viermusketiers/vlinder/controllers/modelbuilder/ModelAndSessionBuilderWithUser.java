package be.viermusketiers.vlinder.controllers.modelbuilder;

import be.viermusketiers.vlinder.entities.user.User;
import be.viermusketiers.vlinder.util.exceptions.NoCandidateAttributeException;
import be.viermusketiers.vlinder.util.exceptions.NoUserLoggedInException;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import java.util.Optional;

public abstract class ModelAndSessionBuilderWithUser extends ModelAndSessionBuilder {
    private static final String USER_ATTRIBUTE_NAME = "user";
    private static final String CANDIDATE_ATTRIBUTE_NAME = "candidate";
    protected final User user;

    public ModelAndSessionBuilderWithUser(Model model, HttpSession session) {
        super(model, session);

        this.user = (User) session.getAttribute(USER_ATTRIBUTE_NAME);
        if (user == null)
            throw new NoUserLoggedInException();
    }

    public User getUser() {
        return this.user;
    }

    public void addUserToModel() {
        model.addAttribute(USER_ATTRIBUTE_NAME, getUser());
    }

    public User getCandidate() {
        return Optional.ofNullable(
                (User) session.getAttribute(CANDIDATE_ATTRIBUTE_NAME)
        ).orElseThrow(NoCandidateAttributeException::new);
    }

    public void addCandidateToModel() {
        model.addAttribute(CANDIDATE_ATTRIBUTE_NAME, getCandidate());
    }
}
