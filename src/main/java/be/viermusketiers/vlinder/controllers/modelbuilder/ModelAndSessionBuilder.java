package be.viermusketiers.vlinder.controllers.modelbuilder;

import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;

public abstract class ModelAndSessionBuilder extends ModelBuilder {
    protected final HttpSession session;

    public ModelAndSessionBuilder(Model model, HttpSession session) {
        super(model);
        this.session = session;
    }
}
