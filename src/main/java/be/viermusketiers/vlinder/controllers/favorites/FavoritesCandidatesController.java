package be.viermusketiers.vlinder.controllers.favorites;

import be.viermusketiers.vlinder.controllers.login.LoginController;
import be.viermusketiers.vlinder.entities.relation.RelationRepository;
import be.viermusketiers.vlinder.entities.relation.RelationType;
import be.viermusketiers.vlinder.entities.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;

@Controller
@RequestMapping("favorites")
public class FavoritesCandidatesController {

    @Autowired
    RelationRepository relationRepository;

    @GetMapping
    public String nextCandidate(Model model, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user == null)
            return LoginController.REDIRECT_LOGIN;
        List<User> likedCandidate = relationRepository.getLikedUsers(user);
        int age = Period.between(user.getDateOfBirth(), LocalDate.now()).getYears();
        model.addAttribute("age", age);
        model.addAttribute("LikedCandidate", likedCandidate);


        return "html/favoritesCandidates";
    }
}
