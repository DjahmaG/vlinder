package be.viermusketiers.vlinder.controllers.login;

import be.viermusketiers.vlinder.entities.user.User;
import be.viermusketiers.vlinder.entities.user.service.UserLoginFinder;
import be.viermusketiers.vlinder.util.exceptions.NoUserFoundException;
import be.viermusketiers.vlinder.util.exceptions.PasswordDoesntMatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private UserLoginFinder userFinder;
    private static final String LOGIN_PAGE = "login/login";
    private static final String PROFILE_PAGE = "redirect:/profile";
    public static final String REDIRECT_LOGIN = "redirect:/login";
    private static final String MESSAGE_ATTRIBUTE_NAME = "message";

    @GetMapping
    public String init(Model model) {
        model.addAttribute(MESSAGE_ATTRIBUTE_NAME, "");
        UserLogIn userLogIn = new UserLogIn();
        model.addAttribute("userLogin", userLogIn);
        return LOGIN_PAGE;
    }

    @PostMapping
    public String submit(Model model, HttpSession session, UserLogIn userLogIn) {
        try {
            addUserToModel(model, session, userLogIn);
            return PROFILE_PAGE;
        } catch (NoUserFoundException ignored) {
            setNoUserMessage(model);
        } catch (PasswordDoesntMatchException ignored) {
            setWrongPasswordMessage(model);
        }

        model.addAttribute("userLogin", userLogIn);
        return LOGIN_PAGE;
    }

    private void addUserToModel(Model model, HttpSession session, UserLogIn userLogIn) {
        User user = userFinder.getUserByUserLogin(userLogIn);
        model.addAttribute("user", user);
        session.setAttribute("user", user);
    }

    private void setNoUserMessage(Model model) {
        model.addAttribute(MESSAGE_ATTRIBUTE_NAME, "No User registerd with that e-mail");
    }

    private void setWrongPasswordMessage(Model model) {
        model.addAttribute(MESSAGE_ATTRIBUTE_NAME, "Password doesn't match the given e-mail");
    }
}
