package be.viermusketiers.vlinder.controllers.startPage;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping({"start","","startpage"})
public class StartPageController {
    @GetMapping
    public String getStartPage() {
        return "html/startPage";
    }
}
