package be.viermusketiers.vlinder.controllers.profileManager;

import be.viermusketiers.vlinder.controllers.login.LoginController;
import be.viermusketiers.vlinder.entities.user.User;
import be.viermusketiers.vlinder.entities.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping({"Profile", "profile"})
public class ProfileManagerController {
    public static final String REDIRECT_PROFILE = "redirect:/profile";
    @Autowired
    private UserRepository userRepository;

    @GetMapping
    public String getProfile(Model model, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user == null)
            return LoginController.REDIRECT_LOGIN;
        model.addAttribute("user", user);
        return "html/profile";
    }

    @PostMapping
    public String updateUser(HttpSession session, User user) {
        User oldUser = (User) session.getAttribute("user");
        oldUser.setGender(user.getGender());
        oldUser.setName(user.getName());
        oldUser.setLocation(user.getLocation());
        oldUser.setDescription(user.getDescription());
        oldUser.setSpecies(user.getSpecies());
        userRepository.saveUser(oldUser);
        return REDIRECT_PROFILE;
    }

}
