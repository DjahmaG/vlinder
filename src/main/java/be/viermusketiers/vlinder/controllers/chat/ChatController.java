package be.viermusketiers.vlinder.controllers.chat;


import be.viermusketiers.vlinder.controllers.chat.modelbuilder.ChatModelAndSessionBuilder;
import be.viermusketiers.vlinder.controllers.chat.modelbuilder.MessageBuilder;
import be.viermusketiers.vlinder.controllers.login.LoginController;
import be.viermusketiers.vlinder.controllers.profileManager.ProfileManagerController;
import be.viermusketiers.vlinder.entities.chat.MessageRepository;
import be.viermusketiers.vlinder.entities.user.User;
import be.viermusketiers.vlinder.entities.user.repository.UserRepository;
import be.viermusketiers.vlinder.util.exceptions.NoUserLoggedInException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("chat")
public class ChatController {
    public static final String MESSAGE_CHAT_BOX = "html/messageChatBoxTest";
    public static final String REDIRECT_CHAT_BOX = "redirect:/chat";
    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping
    public String getMessageFromCandidate(Model model, HttpSession session) {
        try {
            ChatModelAndSessionBuilder sessionBuilder =
                    new ChatModelAndSessionBuilder(model, session, messageRepository);
            sessionBuilder.build();
        } catch (NoUserLoggedInException ignored) {
            return LoginController.REDIRECT_LOGIN;
        }

        return MESSAGE_CHAT_BOX;
    }

    @PostMapping(params = "chat")
    public String saveMessage(@RequestParam String chat, Model model, HttpSession session) {
        try {
            MessageBuilder messageBuilder =
                    new MessageBuilder(model, session, chat, messageRepository);
            messageBuilder.build();
        } catch (NoUserLoggedInException ignored) {
            return LoginController.REDIRECT_LOGIN;
        }

        return REDIRECT_CHAT_BOX;
    }
    @GetMapping("/{id}")
        public String getMessagesById(@PathVariable String id,Model model,HttpSession httpSession){
        User candidate = userRepository.getUserById(Long.parseLong(id));
        if (candidate == null)
            return ProfileManagerController.REDIRECT_PROFILE;
        httpSession.setAttribute("candidate", candidate);

        return REDIRECT_CHAT_BOX;
        }

}
