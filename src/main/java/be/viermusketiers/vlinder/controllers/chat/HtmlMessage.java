package be.viermusketiers.vlinder.controllers.chat;

import be.viermusketiers.vlinder.entities.chat.Message;

public class HtmlMessage extends Message {

    public boolean isSender;

    public HtmlMessage(Message message) {
        super(message.getSender(), message.getRetriever(), message.getSendTime(), message.getText(), message.getStatus());
    }
}
