package be.viermusketiers.vlinder.controllers.chat.modelbuilder;

import be.viermusketiers.vlinder.controllers.chat.HtmlMessage;
import be.viermusketiers.vlinder.controllers.modelbuilder.ModelAndSessionBuilderWithUser;
import be.viermusketiers.vlinder.entities.chat.Message;
import be.viermusketiers.vlinder.entities.chat.MessageRepository;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ChatModelAndSessionBuilder extends ModelAndSessionBuilderWithUser {
    private static final String MESSAGES_ATTRIBUTE_NAME = "chats";
    private final MessageRepository messageRepository;

    public ChatModelAndSessionBuilder(Model model, HttpSession session, MessageRepository messageRepository) {
        super(model, session);
        this.messageRepository = messageRepository;
    }

    @Override
    public void build() {
        addUserToModel();
        addCandidateToModel();

        model.addAttribute(MESSAGES_ATTRIBUTE_NAME, getAllMessagesSortedByTime());
    }

    private List<HtmlMessage> getAllMessagesSortedByTime() {
        List<HtmlMessage> allMessages = new ArrayList<>();
        allMessages.addAll(getReceivedHmlMessages());
        allMessages.addAll(getSentMessages());
        allMessages.sort(Comparator.comparing(Message::getSendTime));

        return allMessages;
    }

    private List<HtmlMessage> getReceivedHmlMessages() {
        List<HtmlMessage> receivedMessages = messageRepository.getMessages(getCandidate(), getUser())
                .stream().map(HtmlMessage::new)
                .collect(Collectors.toList());
        receivedMessages.forEach(htmlMessage -> htmlMessage.isSender=false);
        return receivedMessages;
    }

    private List<HtmlMessage> getSentMessages() {
        List<HtmlMessage> sentMessages = messageRepository.getMessages(user, getCandidate())
                .stream().map(HtmlMessage::new)
                .collect(Collectors.toList());
        sentMessages.forEach(htmlMessage -> htmlMessage.isSender=true);
        return sentMessages;
    }
}
