package be.viermusketiers.vlinder.controllers.chat.modelbuilder;

import be.viermusketiers.vlinder.controllers.modelbuilder.ModelAndSessionBuilderWithUser;
import be.viermusketiers.vlinder.entities.chat.Message;
import be.viermusketiers.vlinder.entities.chat.MessageRepository;
import be.viermusketiers.vlinder.entities.chat.Status;
import be.viermusketiers.vlinder.entities.user.User;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;

public class MessageBuilder extends ModelAndSessionBuilderWithUser {
    private final String messageContent;
    private final MessageRepository messageRepository;

    public MessageBuilder(Model model, HttpSession session, String messageContent, MessageRepository messageRepository) {
        super(model, session);
        this.messageContent = messageContent;
        this.messageRepository = messageRepository;
    }

    @Override
    public void build() {
        User user = getUser();
        User candidate = getCandidate();

        model.addAttribute("user", user);
        Message message = new Message(user, candidate, LocalDateTime.now(), messageContent, Status.SENT);
        messageRepository.saveMessage(message);
    }
}
