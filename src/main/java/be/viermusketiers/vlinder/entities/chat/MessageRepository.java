package be.viermusketiers.vlinder.entities.chat;

import be.viermusketiers.vlinder.entities.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message,Long> {

    @Query(value = "SELECT message FROM Message message WHERE message.sender=:sender and message.retriever=:retriever")
    List<Message> getMessages(User sender,User retriever);

    @Query(value = "SELECT message FROM Message message")
    List<Message> getAllMessages();

    default void saveMessage(Message message) {
        save(message);
    }



    default boolean messageExists(Message message) {
        return getAllMessages().contains(message);
    }
}
