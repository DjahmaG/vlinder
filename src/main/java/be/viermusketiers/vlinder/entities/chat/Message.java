package be.viermusketiers.vlinder.entities.chat;


import be.viermusketiers.vlinder.entities.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.StringJoiner;

@Entity
@Table(name = "Message")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;
    @Column(name = "Content")
    private String text;
    @ManyToOne
    @JoinColumn(name = "Sender")
    private User sender;
    @ManyToOne
    @JoinColumn(name = "Retriever")
    private User retriever;
    @Column(name = "SendTime")
    private LocalDateTime sendTime;
    @Column(name = "Status")
    @Enumerated(EnumType.STRING)
    private Status status ;

    public Message() {
    }

    public Message( User sender, User retriever, LocalDateTime sendTime, String text, Status status) {

        this.sender = sender;
        this.retriever = retriever;
        this.sendTime = sendTime;
        this.text = text;
        this.status = status;
    }



    public long getId() {
        return id;
    }


    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getRetriever() {
        return retriever;
    }

    public void setRetriever(User retriever) {
        this.retriever = retriever;
    }

    public LocalDateTime getSendTime() {
        return sendTime;
    }

    public void setSendTime(LocalDateTime sendTime) {
        this.sendTime = sendTime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Message.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("sender=" + sender)
                .add("retriever=" + retriever)
                .add("sendTime=" + sendTime)
                .add("text='" + text + "'")
                .add("status='" + status + "'")
                .toString();
    }
}
