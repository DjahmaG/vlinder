package be.viermusketiers.vlinder.entities.chat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


public enum Status {
    READ,
    RECEIVED,
    SENDING,
    SENT;
}
