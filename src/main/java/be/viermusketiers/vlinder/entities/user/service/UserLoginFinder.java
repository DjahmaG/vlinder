package be.viermusketiers.vlinder.entities.user.service;

import be.viermusketiers.vlinder.controllers.login.UserLogIn;
import be.viermusketiers.vlinder.entities.user.User;
import be.viermusketiers.vlinder.entities.user.repository.UserRepository;
import be.viermusketiers.vlinder.util.exceptions.NoUserFoundException;
import be.viermusketiers.vlinder.util.exceptions.PasswordDoesntMatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserLoginFinder {
    @Autowired
    private UserRepository userRepository;

    public User getUserByUserLogin(UserLogIn userLogIn) throws NoUserFoundException, PasswordDoesntMatchException {
        User user = userRepository.getUserByEmailOrThrow(userLogIn.getEmail());

        if (user.matchesPassword(userLogIn.getPassword()))
            return user;
        else
            throw new PasswordDoesntMatchException();
    }
}
