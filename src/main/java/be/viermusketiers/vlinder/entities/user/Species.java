package be.viermusketiers.vlinder.entities.user;

public enum Species {
    MAMMALS,
    BIRDS,
    REPTILES,
    AMPHIBIANS,
    FISH,
    INVERTEBRATES
}
