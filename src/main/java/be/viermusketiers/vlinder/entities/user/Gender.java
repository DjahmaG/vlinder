package be.viermusketiers.vlinder.entities.user;

public enum Gender {
    MALE,
    FEMALE,
    HERMAPHRODITE,
    OTHER;

    static Gender getGenderFromString(String gender) {
        for (Gender g : Gender.values()) {
            if (g.name().equalsIgnoreCase(gender))
                return g;
        }
        return OTHER;
    }
}
