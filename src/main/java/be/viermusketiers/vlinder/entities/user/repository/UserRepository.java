package be.viermusketiers.vlinder.entities.user.repository;

import be.viermusketiers.vlinder.controllers.login.UserLogIn;
import be.viermusketiers.vlinder.entities.user.User;
import be.viermusketiers.vlinder.util.exceptions.NoUserFoundException;
import be.viermusketiers.vlinder.util.exceptions.PasswordDoesntMatchException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.sql.Blob;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT user FROM User user")
    List<User> getAllUsers();

    default void saveUser(User user) {
        save(user);
    }
    default void savePicture(User user,Blob blob) {
        Blob pictureToUpload = blob;
       user.setProfilePicture(pictureToUpload);
    }
    @Query(value = "SELECT user FROM User user where user.id = :id")
    User getUserById(Long id);

    default boolean userExists(User user) {
        return getAllUsers().contains(user);
    }

    @Query(value = "SELECT user FROM User user where user.email=:email")
    User getUserByEmail(String email);

    default User getUserByEmailOrThrow(String email) {
        return Optional.ofNullable(getUserByEmail(email))
                .orElseThrow(NoUserFoundException::new);
    }
}
