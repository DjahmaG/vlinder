package be.viermusketiers.vlinder.entities.user.service;
import be.viermusketiers.vlinder.entities.user.User;
import be.viermusketiers.vlinder.entities.user.repository.UserRepository;
import be.viermusketiers.vlinder.util.exceptions.NoMoreUnratedUserException;
import be.viermusketiers.vlinder.entities.relation.Relation;
import be.viermusketiers.vlinder.entities.relation.RelationRepository;
import be.viermusketiers.vlinder.entities.relation.RelationType;
import be.viermusketiers.vlinder.util.RepositoryMaker;

import java.util.*;
import java.util.stream.Collectors;


public class UserMatchMaker {
    private static final RelationRepository relationRepository;
    private static final UserRepository userRepository;
    private final User user;

    static {
        userRepository = RepositoryMaker.getUserRepository();
        relationRepository = RepositoryMaker.getRelationRepository();
    }

    public UserMatchMaker(User user) {
        this.user = user;
    }

    public List<User> getLikedUsers() {
        return relationRepository
                .getRelationsByJudger(user)
                .stream()
                .filter(Relation::isPositive)
                .map(Relation::getJudged)
                .collect(Collectors.toList());
    }

    public List<User> getRatedUsers() {
        return relationRepository
                .getRelationsByJudger(user)
                .stream()
                .map(Relation::getJudged)
                .collect(Collectors.toList());
    }

    public List<User> getUnratedUsers(){
        List<User> allUsers = userRepository.getAllUsers();
        allUsers.remove(user);
        allUsers.removeAll(getRatedUsers());
        return allUsers;
    }

    public User getPotentialCandidate(){
        List<User> unratedUsers = getUnratedUsers();
        return unratedUsers.stream()
                .skip((int) (unratedUsers.size() * Math.random()))
                .findAny().orElseThrow(NoMoreUnratedUserException::new);
    }

    public void judgeUserAndSave(User judged, RelationType judgement){
        Relation relation = new Relation(user, judged, judgement);
        relationRepository.saveRelation(relation);
    }

    public List<User> getMatchedUsers() {
        return getLikedUsers().stream()
                .filter(judgedUser -> relationRepository.userLikesUser(judgedUser, user))
                .collect(Collectors.toList());
    }
}
