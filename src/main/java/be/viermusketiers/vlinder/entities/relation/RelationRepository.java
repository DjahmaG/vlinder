package be.viermusketiers.vlinder.entities.relation;

import be.viermusketiers.vlinder.entities.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RelationRepository extends JpaRepository<Relation, RelationID>{
    @Query(value = "SELECT relation FROM Relation relation")
    List<Relation> getAllRelations();

    @Query(value = "SELECT relation FROM Relation relation WHERE relation.judger = :user")
    List<Relation> getRelationsByJudger (User user);

    @Query(value = "SELECT relation.judged FROM Relation relation " +
            "WHERE relation.judger = :user AND " +
            "(relation.relationType = 'LIKE' OR relation.relationType = 'SUPERLIKE')" )
    List<User> getLikedUsers(User user);

    default boolean userLikesUser(User judger, User judged) {
        return getLikedUsers(judger).contains(judged);
    }

    default void saveRelation(Relation relation) {
        save(relation);
    }
}
