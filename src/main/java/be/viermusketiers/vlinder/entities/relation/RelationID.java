package be.viermusketiers.vlinder.entities.relation;

import be.viermusketiers.vlinder.entities.user.User;

import java.io.Serializable;

public class RelationID implements Serializable {
    private long judger;
    private long judged;

    public RelationID() {
    }

    public RelationID(long judger, long judged) {
        this.judger = judger;
        this.judged = judged;
    }

    public void setJudger(long judger) {
        this.judger = judger;
    }

    public void setJudged(long judged) {
        this.judged = judged;
    }
}
