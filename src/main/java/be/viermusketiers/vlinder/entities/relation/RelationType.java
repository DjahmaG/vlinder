package be.viermusketiers.vlinder.entities.relation;

public enum RelationType {
    LIKE,
    SUPERLIKE,
    DISLIKE,
    IGNORE
}
