package be.viermusketiers.vlinder.entities.relation;

import be.viermusketiers.vlinder.entities.user.User;

import javax.persistence.*;

@Entity
@Table(name = "Relations")
@IdClass(RelationID.class)
public class Relation{
    @Id
    @ManyToOne
    @JoinColumn(name = "Judger", nullable = false)
    private User judger;
    @Id
    @ManyToOne
    @JoinColumn(name = "Judged", nullable = false)
    private User judged;
    @Column(name = "Judgement")
    @Enumerated(EnumType.STRING)
    private RelationType relationType;

    public Relation(){

    }

    public Relation(User judger, User judged, RelationType relationType) {
        this.judger = judger;
        this.judged = judged;
        this.relationType = relationType;
    }

    public User getJudger() {
        return judger;
    }

    public void setJudger(User judger) {
        this.judger = judger;
    }

    public User getJudged() {
        return judged;
    }

    public void setJudged(User judged) {
        this.judged = judged;
    }

    public RelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(RelationType relationType) {
        this.relationType = relationType;
    }

    public boolean isPositive() {
        return relationType.equals(RelationType.LIKE) || relationType.equals(RelationType.SUPERLIKE);
    }
}
