package be.viermusketiers.vlinder.entities.preferences;

import be.viermusketiers.vlinder.entities.user.Gender;
import be.viermusketiers.vlinder.entities.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table (name = "PreferredGender")
public class PreferredGender {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private int ID;

    @ManyToOne
    @JoinColumn(name = "UserID")
    private User user;

    @ElementCollection
    @Column(name = "Gender")
    @Enumerated (EnumType.STRING)
    private List<Gender> likedGender = new ArrayList<>();

    public List<Gender> getLikedGender() {
        return likedGender;
    }
}
