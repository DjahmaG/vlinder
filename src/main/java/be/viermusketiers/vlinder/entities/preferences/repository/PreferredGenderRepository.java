package be.viermusketiers.vlinder.entities.preferences.repository;

import be.viermusketiers.vlinder.entities.preferences.PreferredGender;
import be.viermusketiers.vlinder.entities.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PreferredGenderRepository extends JpaRepository<PreferredGender, Integer> {
    default void savePreferredGender (PreferredGender preferredGender){
        save(preferredGender);
    }

    @Query(value = "SELECT pg FROM PreferredGender pg where pg.user = :user")
    public PreferredGender getGenderPreference(User user);
}
