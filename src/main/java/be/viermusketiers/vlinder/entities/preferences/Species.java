package be.viermusketiers.vlinder.entities.preferences;

public enum Species {
    MAMMALS,
    BIRDS,
    REPTILES,
    AMPHIBIANS,
    FISH,
    INVERTEBRATES
}
