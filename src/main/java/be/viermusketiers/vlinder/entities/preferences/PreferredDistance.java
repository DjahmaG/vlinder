package be.viermusketiers.vlinder.entities.preferences;

import be.viermusketiers.vlinder.entities.user.User;

import javax.persistence.*;

@Entity
@Table (name = "PreferredDistance")
public class PreferredDistance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column (name = "ID", nullable = false)
    private int ID;

    @ManyToOne
    @JoinColumn(name = "UserID")
    private User user;

    @Column (name = "Kilometers")
    private int Kilometers;

    public int getKilometers() {
        return Kilometers;
    }
}
