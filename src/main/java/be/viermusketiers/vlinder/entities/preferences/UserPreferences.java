package be.viermusketiers.vlinder.entities.preferences;

import be.viermusketiers.vlinder.entities.user.Gender;
import be.viermusketiers.vlinder.entities.user.User;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

public class UserPreferences {
    private final User user;
    private final PreferredDistance preferredDistance;
    private final PreferredSpecies preferredSpecies;
    private final PreferredGender preferredGender;

    public UserPreferences(User user, PreferredDistance preferredDistance, PreferredSpecies preferredSpecies, PreferredGender preferredGender) {
        this.user = user;
        this.preferredDistance = preferredDistance;
        this.preferredSpecies = preferredSpecies;
        this.preferredGender = preferredGender;
    }

    public User getUser() {
        return user;
    }

    public int getPreferredMaxDistance() {
        return preferredDistance.getKilometers();
    }
    public List<Species> getLikedSpecies(){
        return preferredSpecies.getLikedSpecies();
    }
    public List<Gender> getLikedGender(){
        return preferredGender.getLikedGender();
    }

    public PreferredDistance getPreferredDistance(){
        return this.preferredDistance;
    }
    public PreferredSpecies getPreferredSpecies(){
        return this.preferredSpecies;
    }
    public PreferredGender getPreferredGender(){
        return this.preferredGender;
    }
}