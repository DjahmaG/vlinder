package be.viermusketiers.vlinder.entities.preferences.repository;

import be.viermusketiers.vlinder.entities.preferences.PreferredDistance;
import be.viermusketiers.vlinder.entities.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PreferredDistanceRepository extends JpaRepository<PreferredDistance, Integer> {
    default void savePreferredDistance(PreferredDistance preferredDistance) {
        save(preferredDistance);
    }

    @Query(value = "SELECT pd FROM PreferredDistance pd where pd.user = :user")
    public PreferredDistance getDistancePreference(User user);
}
