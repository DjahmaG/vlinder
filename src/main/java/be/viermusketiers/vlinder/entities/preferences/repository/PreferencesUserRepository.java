package be.viermusketiers.vlinder.entities.preferences.repository;

import be.viermusketiers.vlinder.entities.preferences.PreferredDistance;
import be.viermusketiers.vlinder.entities.preferences.PreferredGender;
import be.viermusketiers.vlinder.entities.preferences.PreferredSpecies;
import be.viermusketiers.vlinder.entities.preferences.UserPreferences;
import be.viermusketiers.vlinder.entities.user.User;
import be.viermusketiers.vlinder.entities.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

@Component
public class PreferencesUserRepository {
    @Autowired
    private PreferredDistanceRepository preferredDistanceRepository;

    @Autowired
    private PreferredSpeciesRepository preferredSpeciesRepository;

    @Autowired
    private PreferredGenderRepository preferredGenderRepository;
    public UserPreferences getUserPreferences(User user){
        return new UserPreferences(user,
                preferredDistanceRepository.getDistancePreference(user),
                preferredSpeciesRepository.getSpeciesPreference(user),
                preferredGenderRepository.getGenderPreference(user));
    }

    public void saveUserPreferences(UserPreferences userPreferences){
        preferredDistanceRepository.save(userPreferences.getPreferredDistance());
        preferredGenderRepository.save(userPreferences.getPreferredGender());
        preferredSpeciesRepository.save(userPreferences.getPreferredSpecies());
    }
}
