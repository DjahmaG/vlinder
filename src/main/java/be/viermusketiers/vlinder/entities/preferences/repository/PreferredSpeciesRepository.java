package be.viermusketiers.vlinder.entities.preferences.repository;

import be.viermusketiers.vlinder.entities.preferences.PreferredSpecies;
import be.viermusketiers.vlinder.entities.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PreferredSpeciesRepository extends JpaRepository<PreferredSpecies, Integer> {
    default void savePreferredSpecies (PreferredSpecies preferredSpecies){
        save(preferredSpecies);
    }
    @Query(value = "SELECT ps FROM PreferredSpecies ps where ps.user = :user")
    public PreferredSpecies getSpeciesPreference(User user);
}
