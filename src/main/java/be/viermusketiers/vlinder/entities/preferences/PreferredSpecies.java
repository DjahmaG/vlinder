package be.viermusketiers.vlinder.entities.preferences;

import be.viermusketiers.vlinder.entities.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PreferredSpecies")
public class PreferredSpecies {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private int ID;

    @ManyToOne
    @JoinColumn(name = "UserID")
    private User user;

    @ElementCollection
    @Column(name = "Species")
    @Enumerated (EnumType.STRING)
    private List<Species> likedSpecies = new ArrayList<>();

    public List<Species> getLikedSpecies() {
        return likedSpecies;
    }
}
