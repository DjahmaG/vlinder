package be.viermusketiers.vlinder.util;

import be.viermusketiers.vlinder.entities.relation.RelationRepository;
import be.viermusketiers.vlinder.entities.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class RepositoryMaker {
    private static RepositoryMaker instance;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RelationRepository relationRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private RepositoryMaker() {
        instance = this;
    }

    public static UserRepository getUserRepository() {
        return instance.userRepository;
    }

    public static RelationRepository getRelationRepository() {
        return instance.relationRepository;
    }

    public static PasswordEncoder getPasswordEncoder() {
        return instance.passwordEncoder;
    }
}
