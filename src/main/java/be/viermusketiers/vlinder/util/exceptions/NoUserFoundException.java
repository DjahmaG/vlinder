package be.viermusketiers.vlinder.util.exceptions;

public class NoUserFoundException extends RuntimeException {

    public NoUserFoundException() {
    }

    public NoUserFoundException(String message) {
        super(message);
    }
}
