package be.viermusketiers.vlinder.util.exceptions;

public class NoCandidateAttributeException extends RuntimeException {
    public NoCandidateAttributeException() {
    }

    public NoCandidateAttributeException(String message) {
        super(message);
    }
}
