package be.viermusketiers.vlinder.util.exceptions;

public class NoUserLoggedInException extends RuntimeException {
    public NoUserLoggedInException() {
    }

    public NoUserLoggedInException(String message) {
        super(message);
    }
}
