package be.viermusketiers.vlinder.util.exceptions;

import java.util.function.Supplier;

public class NoMoreUnratedUserException extends RuntimeException  {
    public NoMoreUnratedUserException() {
    }

    public NoMoreUnratedUserException(String message) {
        super(message);
    }
}
