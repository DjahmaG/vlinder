/*Drop all*/
DROP TABLE User;
DROP TABLE Image;
DROP TABLE Message;
DROP TABLE Status;

/*Create all*/
CREATE TABLE User
(
    ID          int PRIMARY KEY UNIQUE Auto_Increment,
    Name        varchar(255) NOT NULL,
    Gender      ENUM('MALE','FEMALE','HERMAPHRODITE','OTHER') NOT NULL,
    DateOfBirth date         NOT NULL,
    Species     ENUM('MAMMALS','BIRDS','REPTILES','AMPHIBIANS','FISH','INVERTEBRATES') not null,
    Location    varchar(255) NOT NULL,
    ProfilePic  varchar(255),
    Description varchar(1000),
    Email       varchar(255) NOT NULL UNIQUE,
    Password    varchar(255) NOT NULL
);

CREATE TABLE Image
(
    ID   int PRIMARY KEY UNIQUE NOT NULL auto_increment,
    User int                    NOT NULL,
    FOREIGN KEY (User) references User (ID),
    Link varchar(255)           NOT NULL
);

create table Message
(
    Content   varchar(255),
    Sender    int,
    Retriever int,
    SendTime  time,
    Status    varchar(31),
    FOREIGN KEY (Status) references Status (Status)
);

create table Status
(
    Status varchar(31) PRIMARY KEY
);

CREATE TABLE Relations
(
    Judger    int,
    FOREIGN KEY (Judger) REFERENCES User (ID),
    Judged    int,
    FOREIGN KEY (Judged) REFERENCES User (ID),
    Judgement ENUM('LIKE','SUPERLIKE','DISLIKE','IGNORE'),
    PRIMARY KEY (Judger, Judged)
);
create table verification_token
(
    id          int not null auto_increment primary key,
    user_id     int,
    token       varchar(255),
    expiry_date timestamp,
    foreign key (user_id) references User (id)

);