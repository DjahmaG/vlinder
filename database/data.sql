truncate table User;
truncate table Status;

INSERT INTO User (Name, Gender, DateOfBirth, Species, Location, ProfilePic, Description, Email, Password)
VALUES ('Pingu', 'MALE', '2012-12-12', 'pinguin', 'Antarctica', 'pingu.png', 'very handsome pinguin', 'pingu@hotmail.com', 'pwaappwaap'),
       ('Simba', 'MALE', '2008-10-06', 'lion', 'Congo', 'simba.jpg', 'very dandy lion', 'simba@hotmail.com', 'roarrr'),
       ('Shirley', 'FEMALE', '2016-04-03', 'dog', 'Belgium', 'shirley.jpg', 'diva girl', 'shirley@hotmail.com', 'arfarf'),
       ('Chantal', 'FEMALE', '2017-06-02', 'bird', 'Spain', 'chantal.jpg', 'goofy girl', 'chantal@hotmail.com', 'poink'),
       ('Charles', 'MALE', '2019-09-15', 'frog', 'Denmark', 'charles.jpg', 'smooth guy', 'charles@hotmail.com', 'ribbip');
;

insert into Status(Status)
values('RECEIVED'),('READ'),('SENT'),('SENDING');
